var arr = [];

function themSoN(){
    var n = document.getElementById("txt-nhap-so-n").value*1;
    document.getElementById("txt-nhap-so-n").value = "";
    arr.push(n);
    document.getElementById("txt-them-so-n").setAttribute('value',`${arr}`);
};

function tinhTong(){
    var sum = 0;
    arr.forEach(function(item, index){
        const num = arr[index];
        if(num > 0)
            sum += num;
    });
    document.getElementById("txt-tinh-tong").setAttribute('value', `Tổng số dương: ${sum}`);
};

function demSoDuong(){
    var count = 0;
    arr.forEach(function(item, index){
        const num = arr[index];
        if(num > 0)
            count+=1;
    });
    document.getElementById("txt-dem-so-duong").setAttribute('value', `Số dương: ${count}`);
};


function timSoNhoNhat(){
    var minNum = arr[0];
    for(var index=0; index<arr.length; index++){
        var num = arr[index];
        if(num < minNum)
            minNum = num;
    }
    document.getElementById("txt-tim-so-nho-nhat").setAttribute('value', `Số nhỏ nhất: ${minNum}`);
};


function timSoDuongNhoNhat(){
    var minNumPositive = arr[0];
    arr.forEach(function(item, index){
        var num = arr[index];
        if(num > 0 && num < minNumPositive)
            minNumPositive = num;
    });
    document.getElementById("txt-tim-so-duong-nho-nhat").setAttribute('value', `Số dương nhỏ nhất: ${minNumPositive}`);
};

function timSoChan(){
    function timChan(){
        for(var index = arr.length; index >= 0; index--){
            if(arr[index]%2==0)
                return arr[index];
        }
    }
    document.getElementById("txt-tim-so-chan").setAttribute('value', `Số chẵn cuối cùng: ${timChan()}`);
}


function doiCho(){
    var firstPosition = document.getElementById("txt-vi-tri-1").value;
    var secondPosition = document.getElementById("txt-vi-tri-2").value;

    var temp = arr[firstPosition];
    arr[firstPosition] = arr[secondPosition];
    arr[secondPosition] = temp;

    document.getElementById("txt-doi-cho").setAttribute('value', `Mảng sau khi hoán đổi: ${arr}`);
}

function sapXep(){
    var temp=0;
    for(var i=0; i<arr.length; i++){
        for(var j=i+1; j<arr.length; j++){
            if(arr[i]> arr[j]){
                temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
            }
        }
    }
    document.getElementById("txt-sap-xep-tang").setAttribute('value', `Mảng sau khi sắp xếp: ${arr}`);
}

function timNguyenToDauTien(){
    function kiemTraNguyenTo(n){
        if(n<2)
            return 0;
        for(var i = 2; i < n; i++)
            if(n%i == 0)
                return 0;
        return 1;
    };  
    for(var index = 0; index < arr.length; index++){
        if(kiemTraNguyenTo(arr[index]) == 1){
            document.getElementById("txt-nguyen-to-dau").setAttribute('value', `Số nguyên tố đầu tiên: ${arr[index]}`);
            break;
        };
    };
};


var newArr = []
function themSo(){
    var n = document.getElementById("txt-nhap-so").value*1;
    document.getElementById("txt-nhap-so").value = "";
    newArr.push(n);
    document.getElementById("txt-them-so").setAttribute('value',`${newArr}`);
};

function demSoNguyen(){
    var count = 0;
    newArr.forEach(function(item, index){
        if( Number.isInteger(newArr[index]))
            count +=1;
    });
    document.getElementById("txt-dem-so-nguyen").setAttribute('value',`${count}`);
};

function soSanh(){
    var countPositive = 0, countNegative =0;
    arr.forEach(function(item, index){
        if(arr[index] > 0)
            countPositive+=1;
        else
            countNegative+=1
    });
    if(countPositive > countNegative)
        document.getElementById("txt-so-sanh").setAttribute('value',`Số dương > Số âm`);
    else if(countPositive < countNegative)
        document.getElementById("txt-so-sanh").setAttribute('value',`Số dương < Số âm`);
    else
        document.getElementById("txt-so-sanh").setAttribute('value',`Số dương = Số âm`);
}


